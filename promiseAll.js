var a = new Promise((res, rej) => {
	res('TEST1')
	res('TEST1.5')
})

var b = new Promise((res, rej) => {
	res('TEST2')
})

var c = new Promise((res, rej) => {
	res('TEST3')
})

Promise.all([a,b,c]).then((m) => {
	console.log(m[0])
})