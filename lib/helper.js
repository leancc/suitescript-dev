;
(function (global, factory) {
    (typeof exports === 'object' && typeof module !== 'undefined') ? module.exports = factory():
        (typeof define === 'function' && define.amd) ? define(factory) : global.helper = factory()
}(this, (function () {
    'use strict';

    if (!Array.prototype.findIndex) {
        Object.defineProperty(Array.prototype, 'findIndex', {
            value: function (predicate) {
                // 1. Let O be ? ToObject(this value).
                if (this == null) {
                    throw new TypeError('"this" is null or not defined');
                }

                var o = Object(this);

                // 2. Let len be ? ToLength(? Get(O, "length")).
                var len = o.length >>> 0;

                // 3. If IsCallable(predicate) is false, throw a TypeError exception.
                if (typeof predicate !== 'function') {
                    throw new TypeError('predicate must be a function');
                }

                // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
                var thisArg = arguments[1];

                // 5. Let k be 0.
                var k = 0;

                // 6. Repeat, while k < len
                while (k < len) {
                    // a. Let Pk be ! ToString(k).
                    // b. Let kValue be ? Get(O, Pk).
                    // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
                    // d. If testResult is true, return k.
                    var kValue = o[k];
                    if (predicate.call(thisArg, kValue, k, o)) {
                        return k;
                    }
                    // e. Increase k by 1.
                    k++;
                }

                // 7. Return -1.
                return -1;
            },
            configurable: true,
            writable: true
        });
    }

    if (typeof Object.assign != 'function') {
        // Must be writable: true, enumerable: false, configurable: true
        Object.defineProperty(Object, "assign", {
            value: function assign(target, varArgs) { // .length of function is 2
                'use strict';
                if (target == null) { // TypeError if undefined or null
                    throw new TypeError('Cannot convert undefined or null to object');
                }

                var to = Object(target);

                for (var index = 1; index < arguments.length; index++) {
                    var nextSource = arguments[index];

                    if (nextSource != null) { // Skip over if undefined or null
                        for (var nextKey in nextSource) {
                            // Avoid bugs when hasOwnProperty is shadowed
                            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                                to[nextKey] = nextSource[nextKey];
                            }
                        }
                    }
                }
                return to;
            },
            writable: true,
            configurable: true
        });
    }

    var exports = {}

    exports.parseFloatOrZero = function (a) {
        var b = parseFloat(a)
        return isNaN(b) ? 0 : b;
    }

    exports.addCommas = function (number) {
        if (number) {
            parts = number.toString().split('.');
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            number = parts.join('.');
            return number;
        } else return isNaN(number) ? 0.00 : number;
    }

    exports.formatCurrency = function (n) {
        n = this.parseFloatOrZero(n);
        // n = (!(n%1) != 0) ? n : n;
        n = n != Infinity ? (n != 0 ? ((n < 0) ? '(' + this.addCommas((n * -1).toFixed(2)) + ')' : this.addCommas(n.toFixed(2))) : '0.00') : '0.00';
        return n;
    }

    exports._isEmpty = function (str) {
        return str || '';
    }

    exports._removeNull = function (arr) {
        return arr.filter(function (f) {
            return f != undefined && f != null && f != '';
        });
    }

    exports.wordWrap = function (str) {
        return str ? str.replace(/\n/g, '<br/>').replace(/\x05/g, '<br/>') : '';
    }

    // Remove duplicates
    exports.uniq = function (a) {
        var seen = {};
        return a.filter(function (item) {
            return seen.hasOwnProperty(item) ? false : (seen[item] = true);
        });
    }

    exports.romanizeNum = function (num) {
        var lookup = {
                M: 1000,
                CM: 900,
                D: 500,
                CD: 400,
                C: 100,
                XC: 90,
                L: 50,
                XL: 40,
                X: 10,
                IX: 9,
                V: 5,
                IV: 4,
                I: 1
            },
            roman = '',
            i;
        for (i in lookup) {
            while (num >= lookup[i]) {
                roman += i;
                num -= lookup[i];
            }
        }
        return roman;
    }

    return exports;

})));