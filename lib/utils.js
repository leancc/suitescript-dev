'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
    return typeof obj;
} : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};

/*
 * @Author: leanc
 * @Date:   2018-10-09 05:26:22
 * @Last Modified by: lc
 * @Last Modified time: 2019-10-19 18:52:08
 * @Description: Utility for SuiteScript 2.0
 * 
 * Changelog:
 * - 2019-04-15. Supported shipping address mainline subrecord (created, edit, transform)
 * - 2019-04-16. Supported any mainline subrecords dynamically (created, edit, transform)
 * - 2019-04-17. Added util module
 * - 2019-05-16. Added ignoreMandatoryFields on record submit/save, removed currentTime, currentDate functions
 * - 2019-05-22. Added append of line item for editRecord
 * - 2019-06-07. Added expandSearch function
 * - 2019-07-03. Added pagelogo in companyRecord function
 * - 2019-10-13. Supported removal of non existing lines from serialized data for non dynamic nlobjRecord (editRecord)
 * - 2019-10-16. Added handling for D-M-YYYY, YYYY-D-M formats to date object
 * - 2020-02-02. Added rectype exclusion for itemreceive col field
 */
/**
 *@NApiVersion 2.x
 */

define(['N/runtime', 'N/record', 'N/error', 'N/config', 'N/email', 'N/util'], function (runtime, record, error, config, email, util) {

    var exports = {};

    exports.userPreference = !this.userPreference ? config.load({
        type:config.Type.USER_PREFERENCES
    }) : null;

    exports.getUserDateFormat = function() {
        return this.userPreference.getValue({fieldId: 'DATEFORMAT'})
    }

    /**
     * Validate format and parse to date object
     * @param  {String} string_date String date
     * @return {Object}             Date object
     */
    exports.toDateObject = function(string_date) {
        var dateformat = this.getUserDateFormat()
        if (['D', 'd'].indexOf(dateformat.charAt(0)) > -1) {
            var splitter = (dateformat.indexOf('/') > -1) ? '/' : ((dateformat.indexOf('-') > -1) ? '-' : ((dateformat.indexOf('.') > -1) ? '.' : ' '))
            var splitted = string_date.split(splitter)
            // YYYY MM DD
            if (splitted[1].length == 4)
                return new Date(splitted[0] + '/' + splitted[2] + '/' + splitted[1])
            // MM DD YYYY
            return new Date(splitted[1] + '/' + splitted[0] + '/' + splitted[2])
        }
        return new Date(string_date)
    }

    exports.dateNowByCompanyTimezone = function () {
        var date = new Date();
        var tzt = config.load({
            type: 'companyinformation'
        }).getText({
            fieldId: 'timezone'
        });
        date.setMinutes(date.getMinutes() + date.getTimezoneOffset() + (parseFloat(tzt.substring(tzt.indexOf('+') == -1 ? tzt.indexOf('-') : tzt.indexOf('+'), tzt.indexOf(')')).substring(0, tzt.substring(tzt.indexOf('+') == -1 ? tzt.indexOf('-') : tzt.indexOf('+'), tzt.indexOf(')')).indexOf(':'))) * 60 + (parseFloat(tzt.substring(tzt.indexOf('+') == -1 ? tzt.indexOf('-') : tzt.indexOf('+'), tzt.indexOf(')')).substring(0, tzt.substring(tzt.indexOf('+') == -1 ? tzt.indexOf('-') : tzt.indexOf('+'), tzt.indexOf(')')).indexOf(':'))) < 0 ? parseFloat(tzt.substring(tzt.indexOf('+') == -1 ? tzt.indexOf('-') : tzt.indexOf('+'), tzt.indexOf(')')).substring(tzt.substring(tzt.indexOf('+') == -1 ? tzt.indexOf('-') : tzt.indexOf('+'), tzt.indexOf(')')).indexOf(':') + 1)) * -1 : parseFloat(tzt.substring(tzt.indexOf('+') == -1 ? tzt.indexOf('-') : tzt.indexOf('+'), tzt.indexOf(')')).substring(tzt.substring(tzt.indexOf('+') == -1 ? tzt.indexOf('-') : tzt.indexOf('+'), tzt.indexOf(')')).indexOf(':') + 1)))));
        return date;
    }

    // Sample JSON (obj param key) for createRecord, editRecord, transformRecord function(s)
    // >> {"tranid": "123456 - test 2","entity": 138740,"otherrefnum": "testPO","memo": "TEST123","salesrep": "","department": "15","class": "3","location": "1","custbody_hrx_order_source": "9","custbodycustomer_id": "custid123","custbody_hrx_multi_order": true,"shipmethod": "5499","shippingcost": 30,"shippingaddress": {"country": "GB","attention": "Arrow ECS Australia Pty Ltd","addr1": "nit 24, Hume Ave","addr2": "nit 25, Hume Ave","city":"Dublin","state":"Nairshire","colIdip":2000},"inventorydetail": {"inventoryassignment": [{"issueinventorynumber": "9452","binnumber": "354","quantity": 3}]},"custbody_cc_paid": true,"custbody_cc_paid_amount": "","sublist": {"item": [{"item": "61442-113-01","quantity": 12,"price": -1,"rate": 123.45,"inventorydetail": {"inventoryassignment": [{"issueinventorynumber": "9743","binnumber": "2262","quantity": 3}]},"custcol_special_price": "","custcol_credit_amount": "","custcol_hrx_net_amount": "","custcol_sd": true,"custcol_sd_expiration": "30-Apr-2019","location": ""}, {"item": "62332-075-60","quantity": 3,"price": -1,"rate": 5.00,"inventorydetail": {"inventoryassignment": [{"issueinventorynumber": "9452","binnumber": "354","quantity": 3}]},"custcol_special_price": "","custcol_credit_amount": "","custcol_hrx_net_amount": "","custcol_sd": true,"custcol_sd_expiration": "30-Apr-2019","location": ""},{"item": "65862-584-05","quantity": 1,"price": -1,"rate": 28.48,"inventorydetail": {"inventoryassignment": [{"issueinventorynumber": "9452","binnumber": "354","quantity": 3}]},"custcol_special_price": "","custcol_credit_amount": "","custcol_hrx_net_amount": "","custcol_sd": true,"custcol_sd_expiration": "30-Apr-2019","location": ""}],"salesteam": [{"employee": "139474","contribution": "100","isprimary": true,"issalesrep": true,"salesrole": "-2"}]}} 
    /**
     * @param  {String}  rectype Record type
     * @param  {Object}  obj     Field values (JSON)
     * @param  {Boolean} dynamic Dynamic mode
     * @return {Number}          Record id
     */
    exports.createRecord = function (rectype, obj, dynamic) {
        var rec = record.create({
            type: rectype,
            isDynamic: dynamic
        });
        Object.keys(obj).forEach(function (fieldId) {
            if (fieldId == 'sublist') {
                Object.keys(obj[fieldId]).forEach(function (sublistId ) {
                    var _loop = function _loop(i) {
                        if (!dynamic) {
                            Object.keys(obj[fieldId][sublistId][i]).forEach(function (colId, j) {
                                if (colId.indexOf('inventorydetail') != -1) {
                                    // Create/Edit Subrecord
                                    try {
                                        var j;

                                        (function () {
                                            var subrec = rec.getSublistSubrecord(sublistId, colId, parseInt(i));
                                            var sublistId2 = Object.keys(obj[fieldId][sublistId][i][colId])[0];
                                            for (j in obj[fieldId][sublistId][i][colId][sublistId2]) {
                                                Object.keys(obj[fieldId][sublistId][i][colId][sublistId2][j]).forEach(function (colId2) {
                                                    subrec.setSublistValue(sublistId2, colId2, parseInt(j), obj[fieldId][sublistId][i][colId][sublistId2][j][colId2]);
                                                });
                                            }
                                        })();
                                    } catch (e) {
                                        log.debug('error', e.message);
                                    }
                                } else {
                                    rec.setSublistValue(sublistId, colId, parseInt(i), obj[fieldId][sublistId][i][colId]);
                                }
                            });
                        } else {
                            rec.selectNewLine(sublistId );
                            Object.keys(obj[fieldId][sublistId][i]).forEach(function (colId) {
                                if (colId.indexOf('inventorydetail') != -1) {
                                    // Create/Edit Subrecord
                                    var invDetailReq = rec.getCurrentSublistValue(sublistId, 'inventorydetailreq');
                                    var compDetailReq = rec.getCurrentSublistValue(sublistId, 'componentinventorydetailreq'); // For AB
                                    if (invDetailReq == 'T' || compDetailReq == 'T') {
                                        (function () {
                                            var subrec = rec.getCurrentSublistSubrecord(sublistId, colId);
                                            var sublistId2 = Object.keys(obj[fieldId][sublistId][i][colId])[0];

                                            var _loop2 = function _loop2(j) {
                                                subrec.selectNewLine(sublistId2);
                                                Object.keys(obj[fieldId][sublistId][i][colId][sublistId2][j]).forEach(function (colId2) {
                                                    subrec.setCurrentSublistValue(sublistId2, colId2, obj[fieldId][sublistId][i][colId][sublistId2][j][colId2]);
                                                });
                                                subrec.commitLine(sublistId2);
                                            };

                                            for (var j in obj[fieldId][sublistId][i][colId][sublistId2]) {
                                                _loop2(j);
                                            }
                                        })();
                                    }
                                } else {
                                    rec.setCurrentSublistValue(sublistId, colId, obj[fieldId][sublistId][i][colId]);
                                }
                            });
                            rec.commitLine(sublistId );
                        }
                    };

                    // Sublist ID(s)
                    for (var i in obj[fieldId][sublistId]) {
                        _loop(i);
                    }
                });
            } else if (fieldId != 'sublist' && util.isObject(obj[fieldId]) && !util.isDate(obj[fieldId])) {
                var subrec = rec.getSubrecord(fieldId);
                Object.keys(obj[fieldId]).forEach(function (sublistId, colId2) {
                    subrec.setValue(sublistId, obj[fieldId][sublistId]);

                    var _loop3 = function _loop3(i) {
                        Object.keys(obj[fieldId][sublistId][i]).forEach(function (colId) { // Subrecord sublist
                            subrec.setSublistValue(sublistId, colId, parseInt(i), obj[fieldId][sublistId][i][colId]);
                        });
                    };

                    for (var i in obj[fieldId][sublistId]) {
                        _loop3(i);
                    }
                });
            } else {
                rec.setValue(fieldId, obj[fieldId]);
            }
        });

        var id = rec.save({
            ignoreMandatoryFields: true
        });

        log.debug('Data Created', {
            type: rectype,
            id: id,
            data: obj
        });

        return id;
    };

    /**
     * @param  {String}  rectype        Record type
     * @param  {String}  recid          Record id
     * @param  {Object}  obj            Field values (JSON)
     * @param  {Boolean} dynamic        Dynamic mode
     * @return {Number}                 Record id
     */
    exports.editRecord = function (rectype, recid, obj, dynamic) {
        var rec = record.load({
            type: rectype,
            id: recid,
            isDynamic: dynamic
        });
        Object.keys(obj).forEach(function (fieldId) {
            if (fieldId == 'sublist') {
                Object.keys(obj[fieldId]).forEach(function (sublistId ) {
                    var lc = rec.getLineCount(sublistId);

                    // Remove non existing lines from serialized data
                    if (!dynamic) {
                        for (var i = lc - 1; i >= 0; i--) {
                            try {
                                var col1 = Object.keys(obj[fieldId][sublistId][0])[0];
                                var item = rec.getSublistValue(sublistId, col1, i)
                                var index = obj[fieldId][sublistId].map(function(m) {
                                    return m[col1]
                                }).indexOf(item)
                                if (index == -1) {
                                    rec.removeLine({
                                        sublistId: col1,
                                        line: i
                                    })
                                }
                            } catch(e) {
                                log.debug('error', e.message)
                            }
                        }
                    }
                    
                    var _loop4 = function _loop4(i) {
                        var col1 = Object.keys(obj[fieldId][sublistId][i])[0];
                        var item = obj[fieldId][sublistId][i][col1];
                        var index = rec.findSublistLineWithValue(sublistId, col1, item);
                        log.debug('col1', {recid:recid,col1:col1,item:item,index:index})
                        if (index != -1) {
                            if (!dynamic) {
                                Object.keys(obj[fieldId][sublistId][i]).forEach(function (colId) {
                                    if (colId.indexOf('inventorydetail') != -1) {
                                        var invDetail = rec.getSublistValue(sublistId, 'inventorydetailavail', index);
                                        var invDetailReq = rec.getSublistValue(sublistId, 'inventorydetailreq', index);
                                        var compDetailReq = rec.getSublistValue(sublistId, 'componentinventorydetailreq', index); // For AB

                                        if (invDetail == 'T' || invDetailReq == 'T' || compDetailReq == 'T') {
                                            (function () {
                                                var subrec = rec.getSublistSubrecord(sublistId, colId, index);
                                                var sublistId2 = Object.keys(obj[fieldId][sublistId][i][colId])[0];
                                                var lc2 = subrec.getLineCount(sublistId2);

                                                var _loop5 = function _loop5(j) {
                                                    Object.keys(obj[fieldId][sublistId][i][colId][sublistId2][j]).forEach(function (colId2) {
                                                        try {
                                                            subrec.setSublistValue(sublistId2, colId2, parseInt(j), obj[fieldId][sublistId][i][colId][sublistId2][j][colId2]);
                                                        } catch (e) {
                                                            log.debug('error', e.message);
                                                        }
                                                    });
                                                };

                                                for (var j in obj[fieldId][sublistId][i][colId][sublistId2]) {
                                                    _loop5(j);
                                                }
                                            })();
                                        }
                                    } else {
                                        rec.setSublistValue(sublistId, colId, index, obj[fieldId][sublistId][i][colId]);
                                    }
                                });
                            } else {
                                rec.selectLine(sublistId, parseInt(index));
                                Object.keys(obj[fieldId][sublistId][i]).forEach(function (colId) {
                                    if (colId.indexOf('inventorydetail') == -1) {
                                        rec.setCurrentSublistValue(sublistId, colId, obj[fieldId][sublistId][i][colId]);
                                    }
                                });
                                rec.commitLine(sublistId );
                            }
                        } else {
                            log.debug('ITEM_NOT_EXIST', 'Item does not exist in current record');
                            if (!dynamic) {
                                Object.keys(obj[fieldId][sublistId][i]).forEach(function (colId) {
                                    if (colId.indexOf('inventorydetail') != -1) {
                                        var invDetail = rec.getSublistValue(sublistId, 'inventorydetailavail', index);
                                        var invDetailReq = rec.getSublistValue(sublistId, 'inventorydetailreq', index);
                                        var compDetailReq = rec.getSublistValue(sublistId, 'componentinventorydetailreq', index); // For AB

                                        if (invDetail == 'T' || invDetailReq == 'T' || compDetailReq == 'T') {
                                            (function () {
                                                var subrec = rec.getSublistSubrecord(sublistId, colId, index);
                                                var sublistId2 = Object.keys(obj[fieldId][sublistId][i][colId])[0];
                                                var lc2 = subrec.getLineCount(sublistId2);

                                                var _loop6 = function _loop6(j) {
                                                    Object.keys(obj[fieldId][sublistId][i][colId][sublistId2][j]).forEach(function (colId2) {
                                                        try {
                                                            subrec.setSublistValue(sublistId2, colId2, parseInt(j), obj[fieldId][sublistId][i][colId][sublistId2][j][colId2]);
                                                        } catch (e) {
                                                            log.debug('error', e.message);
                                                        }
                                                    });
                                                };

                                                for (var j in obj[fieldId][sublistId][i][colId][sublistId2]) {
                                                    _loop6(j);
                                                }
                                            })();
                                        }
                                    } else {
                                        rec.setSublistValue(sublistId, colId, lc, obj[fieldId][sublistId][i][colId]);
                                    }
                                });
                            } else {
                                rec.selectNewLine(sublistId );
                                Object.keys(obj[fieldId][sublistId][i]).forEach(function (colId) {
                                    if (colId.indexOf('inventorydetail') != -1) {
                                        // Create/Edit Subrecord
                                        var invDetailReq = rec.getCurrentSublistValue(sublistId, 'inventorydetailreq');
                                        var compDetailReq = rec.getCurrentSublistValue(sublistId, 'componentinventorydetailreq'); // For AB
                                        if (invDetailReq == 'T' || compDetailReq == 'T') {
                                            (function () {
                                                var subrec = rec.getCurrentSublistSubrecord(sublistId, colId);
                                                var sublistId2 = Object.keys(obj[fieldId][sublistId][i][colId])[0];

                                                var _loop7 = function _loop7(j) {
                                                    subrec.selectNewLine(sublistId2);
                                                    Object.keys(obj[fieldId][sublistId][i][colId][sublistId2][j]).forEach(function (colId2) {
                                                        subrec.setCurrentSublistValue(sublistId2, colId2, obj[fieldId][sublistId][i][colId][sublistId2][j][colId2]);
                                                    });
                                                    subrec.commitLine(sublistId2);
                                                };

                                                for (var j in obj[fieldId][sublistId][i][colId][sublistId2]) {
                                                    _loop7(j);
                                                }
                                            })();
                                        }
                                    } else {
                                        rec.setCurrentSublistValue(sublistId, colId, obj[fieldId][sublistId][i][colId]);
                                    }
                                });
                                rec.commitLine(sublistId );
                            }
                        }
                    };

                    // Sublist ID(s)
                    for (var i in obj[fieldId][sublistId]) {
                        _loop4(i);
                    }
                });
            } else if (fieldId != 'sublist' && util.isObject(obj[fieldId]) && !util.isDate(obj[fieldId])) {
                var subrec = rec.getSubrecord(fieldId);
                Object.keys(obj[fieldId]).forEach(function (sublistId, colId2) {
                    subrec.setValue(sublistId, obj[fieldId][sublistId]);

                    var _loop8 = function _loop8(i) {
                        Object.keys(obj[fieldId][sublistId][i]).forEach(function (colId) { // Subrecord sublist
                            subrec.setSublistValue(sublistId, colId, parseInt(i), obj[fieldId][sublistId][i][colId]);
                        });
                    };

                    for (var i in obj[fieldId][sublistId]) {
                        _loop8(i);
                    }
                });
            } else {
                rec.setValue(fieldId, obj[fieldId]);
            }
        });

        var id = rec.save({
            ignoreMandatoryFields: true
        });

        log.debug('Data Edited', {
            type: rectype,
            id: id,
            data: obj
        });

        return id;
    };

    /**
     * @param  {String}  rectype    From record type
     * @param  {String}  recid      From record id
     * @param  {String}  toRectype  Transformed record type
     * @param  {Object}  obj        Field values (JSON)
     * @param  {Boolean} dynamic    Dynamic mode
     * @return {Number}             Record id
     */
    exports.transformRecord = function (rectype, recid, toRectype, obj, dynamic) {
        var rec = record.transform({
            fromType: rectype,
            fromId: recid,
            toType: toRectype,
            isDynamic: dynamic
        });
        Object.keys(obj).forEach(function (fieldId) {
            if (fieldId == 'sublist') {
                Object.keys(obj[fieldId]).forEach(function (sublistId ) {
                    // Sublist ID(s)
                    if (toRectype == record.Type.ITEM_FULFILLMENT || toRectype == record.Type.ITEM_RECEIPT) {
                        var lc = rec.getLineCount(sublistId );
                        for (var i = 0; i < lc; i++) {
                            if (dynamic) {
                                rec.selectLine(sublistId, parseInt(i));
                                rec.setCurrentSublistValue(sublistId, 'itemreceive', false); // untick fulfill / receive
                                rec.commitLine(sublistId);
                            } else {
                                rec.setSublistValue(sublistId, 'itemreceive', parseInt(i), false); // untick fulfill / receive
                            }
                        }
                    }

                    var _loop9 = function _loop9() {
                        var col1 = Object.keys(obj[fieldId][sublistId][i])[0];
                        var item = obj[fieldId][sublistId][i][col1];
                        var index = rec.findSublistLineWithValue(sublistId, col1, item);
                        log.debug('col1', {recid:recid,col1:col1,item:item,index:index})
                        if (index != -1) {
                            if (!dynamic) {
                                Object.keys(obj[fieldId][sublistId][i]).forEach(function (colId) {
                                    if (colId.indexOf('inventorydetail') != -1) {
                                        var invDetailReq = rec.getSublistValue(sublistId, 'inventorydetailreq', index);
                                        var compDetailReq = rec.getSublistValue(sublistId, 'componentinventorydetailreq', index); // For AB
                                        if (invDetailReq == 'T' || compDetailReq == 'T') {
                                            (function () {
                                                var subrec = rec.getSublistSubrecord(sublistId, colId, index);
                                                var sublistId2 = Object.keys(obj[fieldId][sublistId][i][colId])[0];

                                                var _loop10 = function _loop10(j) {
                                                    Object.keys(obj[fieldId][sublistId][i][colId][sublistId2][j]).forEach(function (colId2) {
                                                        // log.debug('subrec.setSublistValue('+sublistId2+', '+colId2+', '+j+', '+obj[fieldId][sublistId][i][colId][sublistId2][j][colId2]+');', obj[fieldId][sublistId][i][colId][sublistId2][j][colId2])
                                                        subrec.setSublistValue(sublistId2, colId2, parseInt(j), obj[fieldId][sublistId][i][colId][sublistId2][j][colId2]);
                                                    });
                                                };

                                                for (var j in obj[fieldId][sublistId][i][colId][sublistId2]) {
                                                    _loop10(j);
                                                }
                                            })();
                                        }
                                    } else {
                                        rec.setSublistValue(sublistId, 'itemreceive', parseInt(i), true); // tick fulfill / receive
                                        rec.setSublistValue(sublistId, colId, index, obj[fieldId][sublistId][i][colId]);
                                    }
                                });
                            } else {
                                rec.selectLine(sublistId, parseInt(index));
                                Object.keys(obj[fieldId][sublistId][i]).forEach(function (colId) {
                                    if (colId.indexOf('inventorydetail') == -1) {
                                        rec.setCurrentSublistValue(sublistId, colId, obj[fieldId][sublistId][i][colId]);
                                    }
                                });
                                rec.commitLine(sublistId );
                            }
                        } else {
                            // return 'Item does not exist in current record';
                            if (!dynamic) {
                                Object.keys(obj[fieldId][sublistId][i]).forEach(function (colId) {
                                    if (colId.indexOf('inventorydetail') != -1) {
                                        var invDetail = rec.getSublistValue(sublistId, 'inventorydetailavail', index);
                                        var invDetailReq = rec.getSublistValue(sublistId, 'inventorydetailreq', index);
                                        var compDetailReq = rec.getSublistValue(sublistId, 'componentinventorydetailreq', index); // For AB

                                        if (invDetail == 'T' || invDetailReq == 'T' || compDetailReq == 'T') {
                                            (function () {
                                                var subrec = rec.getSublistSubrecord(sublistId, colId, index);
                                                var sublistId2 = Object.keys(obj[fieldId][sublistId][i][colId])[0];
                                                var lc2 = subrec.getLineCount(sublistId2);

                                                var _loop6 = function _loop6(j) {
                                                    Object.keys(obj[fieldId][sublistId][i][colId][sublistId2][j]).forEach(function (colId2) {
                                                        try {
                                                            subrec.setSublistValue(sublistId2, colId2, parseInt(j), obj[fieldId][sublistId][i][colId][sublistId2][j][colId2]);
                                                        } catch (e) {
                                                            log.debug('error', e.message);
                                                        }
                                                    });
                                                };

                                                for (var j in obj[fieldId][sublistId][i][colId][sublistId2]) {
                                                    _loop6(j);
                                                }
                                            })();
                                        }
                                    } else {
                                        // var line = (obj[fieldId][sublistId].length-1)+parseInt(i)
                                        // log.debug(colId, {i:i,colId:colId,"obj[fieldId][sublistId][i][colId]":obj[fieldId][sublistId][i][colId]})
                                        rec.setSublistValue(sublistId, colId, parseInt(i), obj[fieldId][sublistId][i][colId]);
                                    }
                                });
                            } else {
                                rec.selectNewLine(sublistId );
                                Object.keys(obj[fieldId][sublistId][i]).forEach(function (colId) {
                                    if (colId.indexOf('inventorydetail') != -1) {
                                        // Create/Edit Subrecord
                                        var invDetailReq = rec.getCurrentSublistValue(sublistId, 'inventorydetailreq');
                                        var compDetailReq = rec.getCurrentSublistValue(sublistId, 'componentinventorydetailreq'); // For AB
                                        if (invDetailReq == 'T' || compDetailReq == 'T') {
                                            (function () {
                                                var subrec = rec.getCurrentSublistSubrecord(sublistId, colId);
                                                var sublistId2 = Object.keys(obj[fieldId][sublistId][i][colId])[0];

                                                var _loop7 = function _loop7(j) {
                                                    subrec.selectNewLine(sublistId2);
                                                    Object.keys(obj[fieldId][sublistId][i][colId][sublistId2][j]).forEach(function (colId2) {
                                                        subrec.setCurrentSublistValue(sublistId2, colId2, obj[fieldId][sublistId][i][colId][sublistId2][j][colId2]);
                                                    });
                                                    subrec.commitLine(sublistId2);
                                                };

                                                for (var j in obj[fieldId][sublistId][i][colId][sublistId2]) {
                                                    _loop7(j);
                                                }
                                            })();
                                        }
                                    } else {
                                        rec.setCurrentSublistValue(sublistId, colId, obj[fieldId][sublistId][i][colId]);
                                    }
                                });
                                rec.commitLine(sublistId );
                            }
                        }
                    };

                    for (var i in obj[fieldId][sublistId]) {
                        _loop9();
                    }
                });
            } else if (fieldId != 'sublist' && util.isObject(obj[fieldId]) && !util.isDate(obj[fieldId])) {
                var subrec = rec.getSubrecord(fieldId);
                Object.keys(obj[fieldId]).forEach(function (sublistId, colId2) {
                    subrec.setValue(sublistId, obj[fieldId][sublistId]);

                    var _loop11 = function _loop11(i) {
                        Object.keys(obj[fieldId][sublistId][i]).forEach(function (colId) { // Subrecord sublist
                            subrec.setSublistValue(sublistId, colId, parseInt(i), obj[fieldId][sublistId][i][colId]);
                        });
                    };

                    for (var i in obj[fieldId][sublistId]) {
                        _loop11(i);
                    }
                });
            } else {
                rec.setValue(fieldId, obj[fieldId]);
            }
        });

        var id = rec.save({
            ignoreMandatoryFields: true
        });

        log.debug('Data transformed', {
            from: {
                type: rectype,
                id: recid
            },
            to: {
                type: toRectype,
                id: id
            },
            data: obj
        });

        return id;
    };

    exports.expandSearch = function (set) {
        var searchResults = set.run();
        var resultIndex = 0;
        var resultStep = 1000;
        var resultSet = void 0;
        var resultSets = [];

        do {
            resultSet = searchResults.getRange(resultIndex, resultIndex + resultStep);
            resultSets = resultSets.concat(resultSet);
            resultIndex = resultIndex + resultStep;
        } while (resultSet.length > 0);

        return resultSets;
    }

    exports.companyRecord = function () {
        var company = config.load({
            type: 'companyinformation'
        });
        var ids = ['companyname', 'pagelogo', 'mainaddress_text', 'employerid', 'fax', 'pagelogo', 'phone', 'url', 'email'];
        var obj = {};
        for (var i in ids) {
            obj[ids[i]] = company.getValue(ids[i]) || '';
        }
        // Fields inside address subrecord
        var addressRecord = company.getSubrecord('mainaddress');

        ids = ['country', 'addrphone', 'addrtext', 'addr1'];
        for (var i in ids) {
            obj[ids[i]] = addressRecord.getValue(ids[i]) || '';
        }
        return obj;
    }

    exports.notifyCurrentUser = function (title, message) {
        var user = runtime.getCurrentUser().id;
        email.send({
            author: user,
            recipients: [user],
            subject: title,
            body: function (m) {
                if (typeof m == 'string') return m;
                if ((typeof m === 'undefined' ? 'undefined' : _typeof(m)) == 'object') return JSON.stringify(m);
            }(message)
        });
    }

    return exports
});